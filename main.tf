terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {}
}

provider "aws" {
  version = "~> 2.8"
  region  = var.region
}

data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.15.0"

  name               = "${var.product}-${var.environment}"
  cidr               = var.vpc_cidr
  azs                = slice(data.aws_availability_zones.available.names, 0, 3)

  private_subnets    = [ "${cidrsubnet(var.vpc_cidr, 4, 0)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 1)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 2)}"
                       ]

  public_subnets     = [ "${cidrsubnet(var.vpc_cidr, 4, 10)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 11)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 12)}"
                       ]

  enable_nat_gateway = var.vpc_enable_nat_gateway

  tags = {
    Environment = var.environment
    Product     = var.product
  }
}

