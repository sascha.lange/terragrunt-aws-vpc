variable "region" {
    description = "Region for resources"
    type = string
}

variable "environment" {
    description = "Environment type"
    type        = string
}

variable "product" {
    description = "Product name"
    type        = string
}

variable "vpc_cidr" {
  description = "Main CIDR Block for VPC"
  type        = string
}

variable "vpc_enable_nat_gateway" {
  description = "Enable NAT Gateway for Private Subnets"
  type        = bool
  default     = true
}

